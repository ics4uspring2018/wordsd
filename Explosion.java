import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * @author (WordsD) 
 * 
 * Description:
 * the explosion effect for the planes
 */
public class Explosion extends Actor
{
    private int frame;
    
    private GreenfootImage Ex1;
    private GreenfootImage Ex2;
    private GreenfootImage Ex3;
    private GreenfootImage Ex4;
    private GreenfootImage Ex5;
    private GreenfootImage Ex6;
    private GreenfootImage Ex7;
    private GreenfootImage Ex8;
    private GreenfootImage Ex9;
    private GreenfootImage Ex10;
    private GreenfootImage Ex11;
    private GreenfootImage Ex12;
    private GreenfootImage Ex13;
    
    /**
     * Act - do whatever the Explosion wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        animation();
    }
    
    public Explosion() 
    {
        //set to initial value
        frame = 0;
        
        Ex1 = new GreenfootImage("Ex-0.gif");
        Ex2 = new GreenfootImage("Ex-1.gif");
        Ex3 = new GreenfootImage("Ex-2.gif");
        Ex4 = new GreenfootImage("Ex-3.gif");
        Ex5 = new GreenfootImage("Ex-4.gif");
        Ex6 = new GreenfootImage("Ex-5.gif");
        Ex7 = new GreenfootImage("Ex-6.gif");
        Ex8 = new GreenfootImage("Ex-7.gif");
        Ex9 = new GreenfootImage("Ex-8.gif");
        Ex10 = new GreenfootImage("Ex-9.gif");
        Ex11 = new GreenfootImage("Ex-10.gif");
        Ex12 = new GreenfootImage("Ex-11.gif");
        Ex13 = new GreenfootImage("Ex-12.gif");
    }
    
    public void animation()
    {
        frame = frame + 1;
        String fileName = ("Ex-"+frame/2+".gif");
        setImage(new GreenfootImage(fileName));
        //if frame reaches 24 then remove the explosion
        if(frame == 24)
        {
            getWorld().removeObject(this);
        }
    }
}
