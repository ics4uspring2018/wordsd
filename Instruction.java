import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * @author (WordsD Group) 
 * 
 * Description:
 * the instruction of the game, if the player knows all the rules then start the game
 */
public class Instruction extends World
{
    private ScoreBoard title;
    private ScoreBoard t1;

    private static StartButton intro;
    private static InsButton ins;

    public static boolean modeL = false, modeS = true;
    
    private boolean start;
    
    public static boolean hard;

    private String key;
    
    //background music for the instruction world
    private instructionBGM ibgm;
    
    public static final Color TRANSPARENT = new Color(0, 0, 0, 0);
    
    Text modeE = new Text ("Easy", 30);
    Text modeD = new Text ("Difficult", 30 );
    /**
     * Constructor for objects of class Instruction.
     * 
     */
    public Instruction()
    {    
        // Create a new world with 500X750 cells with a cell size of 1x1 pixels.
        super(500, 750, 1); 
        
        //sound effect
        ibgm = new instructionBGM();
        addObject(ibgm, 0, 0);
        
        //Title and message
        title = new ScoreBoard(450, 100);
        addObject(title, 250, 65);
        title.updateTitle("Space Typer");
        
        t1 = new ScoreBoard(450, 20);
        addObject(t1, 250, 130);
        t1.update("Hello! Welcome to Space Typer!");
        
        addObject(modeE, 150, 650);
        addObject(modeD, 350, 650);
       
        ins = new InsButton();
        addObject(ins, 250 , 400);
        intro = new StartButton();
        addObject(intro, 250, 520);
    }
    
    public void act()
    {
        if(Greenfoot.mouseClicked(intro))
        {
            instructionBGM IBGM = new instructionBGM();
            IBGM.stopS();//stop the background music for the instruction world
            start = true;//start the game
        }

        //if start then start the actual game
        if(start) 
        {
            startGame();
        }
        
        if(Greenfoot.mouseClicked(ins))
        {
            Ins i = new Ins();
            Greenfoot.setWorld(i);
        }
            
        if (Greenfoot.mouseClicked(modeE))
        {
            modeS = !modeS;
            modeL = !modeL;
        }
        switchImage(modeS, modeE, "Easy");
        
        if (Greenfoot.mouseClicked(modeD))
        {
            modeL = !modeL;
            modeS = !modeS;
        }
        switchImage(modeL, modeD, "Difficult");
    }

    private void startGame()
    {
        MyWorld mw = new MyWorld();
        Greenfoot.setWorld(mw);
    }
    
    public void switchImage(boolean m, Text mode, String stringMode)
    {
        if(!m)
        {
            mode.setImage(new GreenfootImage(stringMode + " (OFF)", 20, Color.LIGHT_GRAY, TRANSPARENT));
        }
        else
        {
            mode.setImage(new GreenfootImage(stringMode + " (ON)", 30, Color.WHITE, TRANSPARENT));
        }
    }
}
