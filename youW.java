import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * @author (WordsD) 
 * 
 * Description:
 * create a youwin image to show that the player win the game, there will also be a 
 * sound effect when the image appear
 */
public class youW extends Actor
{
    public youW()
    {
        //resize the image
        getImage().scale(550, 200); 
    }
    /**
     * Act - do whatever the youW wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {    
        
    }    
}
