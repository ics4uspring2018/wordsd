import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * @author (WordsD) 
 * 
 * Description:
 * create a gameover image to show that the player lose the game, there will also be a 
 * sound effect when the image appear
 */
public class gameO extends Actor
{
    public gameO()
    {
       getImage().scale(500, 250); 
    }
    /**
     * Act - do whatever the Reload wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        
    }     
}
