import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Instructions for the game
 * 
 * @author (WordsD) 
 */
public class Ins extends World
{
    private ScoreBoard title;
    private ScoreBoard t1;
    private ScoreBoard t2;
    private ScoreBoard t3;
    private ScoreBoard t4;
    private ScoreBoard t5;
    private ScoreBoard t6;
    private ScoreBoard t7;
    private ScoreBoard t8;
    private static Back back;

    public Ins()
    {
        super(500, 750, 1); 
        
        title = new ScoreBoard(450, 100);
        addObject(title, 250, 65);
        title.updateTitle("Space Typer");
        
        t1 = new ScoreBoard(450, 20);
        addObject(t1, 250, 130);
        t1.update("Hello! Welcome to Space Typer!");
        
        t2 = new ScoreBoard(450, 20);
        addObject(t2, 250, 210);
        t2.update("Here Are All The Instructions: ");
        
        t3 = new ScoreBoard(450, 20);
        addObject(t3, 250, 290);
        t3.update("Oh No, Ninja Space Assault");
        
        t4 = new ScoreBoard(450, 20);
        addObject(t4, 250, 370);
        t4.update("Type Words");
        
        t5 = new ScoreBoard(450, 20);
        addObject(t5, 250, 450);
        t5.update("Backspace to Undo");
        
        t6 = new ScoreBoard(450, 20);
        addObject(t6, 250, 530);
        t6.update("Enter to Submit Word");
        
        t7 = new ScoreBoard(450, 20);
        addObject(t7, 250, 610);
        t7.update("If 3 oponent's planes hit our base aircraft, then GAMEOVER!!!!!");
        
        t8 = new ScoreBoard(450, 20);
        addObject(t8, 250, 630);
        t8.update("MORE and MORE enemy planes are coming!");
        
        back = new Back();
        addObject(back, 100, 700);
    }
    
    public void act()
    {
        if(Greenfoot.mouseClicked(back))
        {
            Instruction a = new Instruction();
            Greenfoot.setWorld(a);
        }
    }
}
