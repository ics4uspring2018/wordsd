import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.io.*;
import java.util.*;
/**
 * Write a description of class Text here.
 * 
 * @author (WordsD) 
 */
public class ReadText extends Actor
{
    /**
     * Act - do whatever the Text wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public ArrayList<String> readText() throws IOException
    {
        InputStream file = getClass().getClassLoader().getResourceAsStream("nouns.txt");
        BufferedReader read = new BufferedReader(new InputStreamReader(file));
        ArrayList<String> nouns = new ArrayList<String>();
        int i = 0;
        if (read.readLine() != null) 
        {
            nouns.add(read.readLine());
            i++;
        }
        return nouns;
    }  
}
