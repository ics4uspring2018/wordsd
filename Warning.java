import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * @author (Cindy Cao) 
 * @version (create date: Jan 17, 2018))
 * 
 * Description:
 * since if 10 enemy planes go out of sight, the game will also count as gameover, so the 
 * flashing warning sign appears when there are 8 enemy planes went out of the sight already,
 * it is here to warn the player that stop letting the enemy planes went out of the sight, you will 
 * lose the game. 
 */
public class Warning extends Actor
{
    private int act;
    private boolean removeMe;//whether to remove the warning sign from the world

    public Warning()
    {
        this.getImage().scale(250,200); //resize the image
        getImage().setTransparency(0); //define the visibility of the image as clear
    }

    /**
     * Act - do whatever the Warning wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        act++;
        //if the sign should not be removed, then run show
        if(!removeMe)
            show();
        // if the sign should be removed, then remove it from the world
        if(removeMe)
            getWorld().removeObject(this);
        //if gameover or player wins, then remove the warning sign
        if(MyWorld.youWin)
            getWorld().removeObject(this);
        if(MyWorld.gameOver)
            getWorld().removeObject(this);
    }
    
    private void show()
    {
        //if runs more than 20 times, then remove the sign
        if(act > 20)
        {
            removeMe = true;
        }
        if(!removeMe)
        {
            //make it flash
            if(act >= 0 && act <= 10)
                getImage().setTransparency(act+50);
            else
                getImage().setTransparency(10-(act-11));
        }
    }
}
