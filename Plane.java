import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * @author (wordsd) 
 * @version (create date: Dec 15, 2017)
 * 
 * Description:
 * Construct a player plane actor that can move the plane and shoot bullets to remove the
 * enemy's planes from the world. 
 */
public class Plane extends Actor
{
    private int shootBullet;//count the number of bullet fired
    
    //after the bulletReload number of bullet fired, the plane need to reload once
    private int bulletReload;
    
    //determine whether the plane overlaped with another image
    public static boolean meetPlane;
    
    //count number of times the plane got hit
    public static int removeMe;
    /**
     * Act - do whatever the Plane wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */

    public Plane()
    {
        //resize the image
        getImage().scale(480, 150);
        
        //set to false first, since plane didn't meet anyting yet
        meetPlane = false;

        //set removeMe as 3, because the player have 3 lifes, so the plane can only got hit 3 times
        removeMe = 3;
    }

    public void act() 
    {
        //set a detect KeyBoard, so the game can detect the keys that the user press
        detectKeyBoard();
    }    
    
    private void detectKeyBoard()
    {
        Enemy e = (Enemy)getOneIntersectingObject(Enemy.class);
        if(e != null)
        {
            e.meetEnemy();
            MyWorld.addPoint();
        }
        //if the plane touched either enemy's plane or bullet, then the player will lost a heart
        if(meetPlane)
        {
            removeMe--;
            MyWorld.removeHeart();
            meetPlane = false;
        }
        //if the plane fo no have any life, then explode
        if(removeMe == 0)
        {
            getWorld().addObject(new Explosion(),getX(),getY());
            ExplodeSound.Sound();
            getWorld().removeObject(this);
        }
    }
}