import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * @author (WordsD) 
 * 
 * Description:
 * the background music for the actual game, play the music when start playing, and ends
 * when gameover or the player win the game.
 */
public class BGM extends Actor
{
    //soundtrack
    private static GreenfootSound m = new GreenfootSound("My Demons.mp3");
    /**
     * Act - do whatever the BGM wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        //if the music is not playing then play it 
        if(!m.isPlaying())
        {
            m.play();
            m.setVolume(80);
        }
        //if the player win the game, stop the music
        if(MyWorld.youWin)
        {
            m.stop();
        }
        //if the player lose the game, stop the music 
        if(MyWorld.gameOver)
        {
            m.stop();
        }
    }
}
