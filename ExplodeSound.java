import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * @author (WordsD) 
 * 
 * Description:
 * the sound effect for explosion (when a explosion appeared, play the sound effect)
 */
public class ExplodeSound extends Actor
{
    //whether should play sound
    public static boolean PlaySound;
    //soundtrack
    private static GreenfootSound explodeS = new GreenfootSound("ExplodeSound.mp3");
    
    public ExplodeSound()
    {
        //do not play the sound first
        PlaySound = false;
    }
    /**
     * Act - do whatever the Reload wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if(PlaySound)
        {
            explodeS.play();
            explodeS.setVolume(80);
            ns();//no sound
        }
    }    
    //play sound
    public static void Sound()
    {
        PlaySound  = true;
    }
    //do not play sound
    private void ns()
    {
        PlaySound = false;
    }
}
