import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
/**
 * @author (WordsD) 
 * 
 * Description:
 * display the scorebar, and update the value as the game goes
 */
public class ScoreBoard extends Actor
{
    /**
     * Act - do whatever the ScoreBoard wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */

    // Declare Objects
    private GreenfootImage scoreBoard;
    private Color bg;
    private Color content;
    private Font font;
    private String text;
    private final int MAX = 500;

    // Declare Variables:
    private int width;
   
    //scoreboard setup(font, color...)
    public ScoreBoard(int width, int height)
    {
        if(height == 100)
        {
            scoreBoard = new GreenfootImage (width, 40);
            content = new Color(255, 255, 255);
            font = new Font ("Bank Gothic", true, true, 40);
        }
        else if(height == 0)
        {
            scoreBoard = new GreenfootImage (width, 30);
            content = new Color(148, 49, 38);
            font = new Font ("New Time Roman", false, true, 20);
        }
        else
        {
            scoreBoard = new GreenfootImage (width, height);
            content = new Color(255, 255, 255);
            font = new Font ("Futura", false, true, 15);
        }
        
        bg = new Color (123, 125, 125);
        scoreBoard.setColor(bg);
        scoreBoard.fill();
        scoreBoard.setFont(font);
        scoreBoard.setColor(content);  
        this.setImage (scoreBoard);
        this.width = width;
    }

    /**
     * Updates this ScoreBar when game starts.
     * Re-write each value changed while game it playing
     * 
     * @param shot   number of bullet shot
     * @param max   max number of bullet
     * @param dead   number of dead enemy
     * @param point    score  
     */
    public void update (int score)
    {
        text = "Score: "+score+" / "+MAX;
        this.update (text, 11);
    }

    public void updateTitle (String output)
    {
        // Refill the background with background color        
        scoreBoard.setColor(bg);
        scoreBoard.fill();

        // code that centers text
        scoreBoard.setColor(content);
        int centeredY = (width/2) - ((output.length()*23)/2);

        // Draw the text onto the image
        scoreBoard.drawString(output, centeredY, 32);
    }
    
    /**
     * Takes a String and displays it centered to the screen.
     * 
     * @param output    Text to display. 
     */
    public void update (String output)
    {
        // Refill the background with background color        
        scoreBoard.setColor(bg);
        scoreBoard.fill();
        
        // Write text over the solid background
        scoreBoard.setColor(content);
        
        // code that centers text
        int centeredY = (width/2) - ((output.length() * 7)/2);

        // Draw the text onto the image
        scoreBoard.drawString(output, centeredY, 16);
    }

    /**
     * display the String given in the centered, only used for the score when started the game
     * 
     * @param output   the text that will be displayed
     * @param one   to differentiate with the update method that was used for the text messages in 
     *              Instruction World
     */
    public void update (String output, int one)
    {
        // Refill the background with background color        
        scoreBoard.setColor(bg);
        scoreBoard.fill();
        // code that centers text
        int centeredY = (width/2) - ((output.length() * one)/4);
        scoreBoard.setColor(content);
        // Draw the text onto the image
        scoreBoard.drawString(output, centeredY, 24);
        this.setImage(scoreBoard);
    }
}
