import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * @author (WordsD) 
 * 
 * Description:
 * construct a enemy plane to move towrds the player and shooting bullets. Both of the 
 * plane and bullet can damage the player. But whenever the enemy got hit by the player,
 * then it will distory. 
 */
public class Enemy extends Actor
{
    //count acts
    private int count;
    //count number of times the plane got hit
    public boolean removeMe;
    //number of enemies that went of the sight
    private int numOfEnemyPass;
    public Enemy()
    {
        //set these variables to the initial value
        count = 0;
        numOfEnemyPass = 0; 
        //set to false first
        removeMe = false;
        //resize the image
        getImage().scale(40, 50);
    }
    
    /**
     * Act - do whatever the Enemy wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        //count act everytime it runs
        count++;
        //assign a new location for the enemies
        setLocation(this.getX(), this.getY()+1);
        //every 100 act, update the score bar once
        //remove the enemy if it touched the player plane
        Plane p = (Plane)getOneIntersectingObject(Plane.class);
        if(p != null)
        {
            Plane.meetPlane = true;
            meetEnemy();
            MyWorld.bombB = true;
        }
    }  
    //after enemy touched something, it will be removed and an explosion will show
    public void meetEnemy()
    {
        getWorld().addObject(new Explosion(),getX(),getY());
        ExplodeSound.Sound();
    }
}
