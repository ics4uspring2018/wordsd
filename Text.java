import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Text here.
 * 
 * @author (WordsD) 
 */
public class Text extends Actor
{
    /**
     * Act - do whatever the Text wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public Text(String text, int size)
    {
        if (size != 0)
        {
            GreenfootImage image = new GreenfootImage(text, size, Color.WHITE, MyWorld.TRANSPARENT);
            setImage(image);
        }
        else
        {
            GreenfootImage image = new GreenfootImage(text);
            setImage(image);
        }
    }
}
