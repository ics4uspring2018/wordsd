import greenfoot.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.InputStream;
import java.util.*;
import java.math.*;
import java.io.*;
import java.net.URL;

/**
 * @author (Words D group) 
 * @version (Apr 13, 2018)
 * @game name (Space Typer)
 * 
 * 
 */
public class MyWorld extends World
{
    //General
    private boolean backspacePressed = false;
    private ScoreBoard score;
    private static Plane plane;
    private Enemy enemy;
    private static gameO go;
    private static youW yw; 

    //score varlable
    private static int scoreP;
    private int action;

    //sound variables
    private ExplodeSound es;
    private BGM bgm;
    private youWSound yws; 
    private gameOSound gos;
    private TypingSound ts;

    //determine whether the player win or loss the game
    public static boolean gameOver;
    public static boolean youWin;

    //health variables
    private static Health h1; 
    private static Health h2; 
    private static Health h3; 

    //determine if the player should lost a heart
    public static boolean removeH1;
    public static boolean removeH2;
    public static boolean removeH3;
    
    //Bomb
    public static boolean bomb;
    public static boolean bombB;
    
    //Input
    public static final Color TRANSPARENT = new Color(0, 0, 0, 0);
    String input, iinput;
    private String[] allowedKeys = {"q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j",
            "k","l","z","x","c","v","b","n","m","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","0","1","2","3","4","5","6","7","8","9","`","'","-"," "};

    ArrayList<String> lines = new ArrayList<String>();
    BufferedReader br = null;
    private String first;
    
    //Random
    private int wRand;
    private int wRand2;
    private int rand;
    
    // others
    public static int eX;
    private ScoreBoard t9;
    private boolean oneDown;
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {
        super(500, 750, 1);

        //add the player to the world
        plane = new Plane();
        addObject(plane, 250, 650);

        //add the score bar to show the score
        score = new ScoreBoard(250, 0);
        addObject(score, 50, 20);

        //add the hearts for health to the assigned coordinate
        h1 = new Health();
        addObject(h1, 30, 730);
        h2 = new Health();
        addObject(h2, 85, 730);
        h3 = new Health();
        addObject(h3, 140, 730);

        //sound affects
        bgm = new BGM();
        addObject(bgm, -1, -1);
        es = new ExplodeSound();
        addObject(es, -1, -1);
        gos = new gameOSound();
        addObject(gos, -1, -1);
        yws = new youWSound();
        addObject(yws, -1, -1);
        ts = new TypingSound();
        addObject(ts, -1, -1);

        //set these variables to 0 at the beginning of the game
        action = 0;
        scoreP = 0; 

        //set these varibles to false
        gameOver = false;
        youWin = false;
        removeH1 = false;
        removeH2 = false;
        removeH3 = false;
        bomb = false;
        bombB = false;

        //set the world as the instruction world first
        Instruction i = new Instruction();
        Greenfoot.setWorld(i);

        try
        {
            InputStream inputs = getClass().getClassLoader().getResourceAsStream("nouns.txt");
            br = new BufferedReader(new InputStreamReader(inputs));
        }
        catch(Exception e)
        {
            System.out.println("file missing");
            return;
        }
        try
        {
            String line = null;
            while ((line = br.readLine()) != null) 
            {
                lines.add(line);
            }
        }
        catch(Exception e)
        {
            try
            {
                br.close();
            } 
            catch(Exception f)
            {}
        }
        if(Instruction.modeS)
        {
            wRand = Greenfoot.getRandomNumber(899);
        }
        if(Instruction.modeL)
        {
            wRand = Greenfoot.getRandomNumber(190)+900;
        }
        first = lines.get(wRand);
        placeText(first, 150,100,30);
    }
    
    Queue<Enemy> ene = new LinkedList<Enemy>();
    
    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    public void act()
    {
        //create enemies (increasing difficulties)
        if(scoreP <= 100){
            rand = Greenfoot.getRandomNumber(160);
            spawnEne();
        }
        if(scoreP >100 && scoreP<=200){
            rand = Greenfoot.getRandomNumber(130);
            spawnEne();
        }
        if(scoreP >200 && scoreP<=300){
            rand = Greenfoot.getRandomNumber(105);
            spawnEne();
        }
        if(scoreP >300 && scoreP<=400){
            rand = Greenfoot.getRandomNumber(95);
            spawnEne();
        }
        if(scoreP >400 && scoreP<=500){
            rand = Greenfoot.getRandomNumber(85);
            spawnEne();
        }
        //count act everytime it runs
        action++;
        //every 25 act, update the score bar once
        if(action%25 == 0)
        {
            score.update(scoreP);
        }
        //remove the hearts for health 
        if(removeH1)
            removeObject(h1);
        if(removeH2)
            removeObject(h2);
        if(removeH3)
            removeObject(h3);

        //if score is at 500, then game stop and player wins
        if(scoreP >= 500)
        {
            Greenfoot.stop();//stop the game
            youWSound.Sound();//add the you win sound effect
            yw = new youW();
            addObject(yw, 230, 370);//add the "You win!" picture to the assigned coordinate
        }
        
        //if gameover, then game stop and player lose (Gameover!)
        if(gameOver)
        {
            //stop the game
            gameOSound.Sound();//add the gameover sound effect
            go = new gameO();
            addObject(go, 250, 370);//add the "Gameover!" picture to the assigned coordinate
            t9 = new ScoreBoard(450, 20);
            addObject(t9, 250, 630);
            t9.update("(Press space to go back to the menu.)");
            if(Greenfoot.isKeyDown("space"))
            {
                Greenfoot.setWorld(new Instruction());
            }
        }

        //Enemy destroyed by typing
        if (bomb && !ene.isEmpty()) 
        {   addObject(new Explosion(),ene.peek().getX(),ene.peek().getY());
            ExplodeSound.Sound();
            removeObject(ene.peek());
            ene.remove();
            addPoint();
            bomb = false;
        }
        
        //Enemy destroyed by hitting
        if (bombB && !ene.isEmpty()) 
        {   addObject(new Explosion(),ene.peek().getX(),ene.peek().getY());
            ExplodeSound.Sound();
            removeObject(ene.peek());
            ene.remove();
            bombB = false;
        }
        /*
         * Getting Input (Words part)
         * 
         */
        if(!gameOver){
            input = Greenfoot.getKey(); 
            if (Arrays.asList(allowedKeys).contains(input) && input.length() == 1)
            {
                if (iinput == null) 
                {
                    iinput = "";
                }
                iinput += input;//accumulating user input in iinput
                TypingSound.Sound();
                placeText(iinput, 390, 600, 30);
                //showing input on the screen
            }  
            if(Greenfoot.isKeyDown("backspace")){
                while(!backspacePressed){ // check if backspace was pressed already
                    if(iinput.length() != 0){ // makes sure that the String is not null
                        iinput = iinput.substring(0, iinput.length() - 1); //deletes the last letter of the String
                    }
                    backspacePressed = true; // makes the boolean true so that the backspace does not happen multiple times
                }
                TypingSound.Sound();
                placeText(iinput, 390, 600, 30); // displays text
            }
            if(!Greenfoot.isKeyDown("backspace"))
            {
                backspacePressed = false; // makes the boolen false again for the next time needed to backspace
            }
            if(Greenfoot.isKeyDown("enter"))
            {
                TypingSound.Sound();
                if(Instruction.modeS){
                    wRand2 = Greenfoot.getRandomNumber(899);
                }
                if(Instruction.modeL){
                    wRand2 = Greenfoot.getRandomNumber(190)+900;
                }
                if(iinput.equals(first)){
                    bomb = true;
                    first = lines.get(wRand2);
                    placeText(first, 150 ,100,30);
                    iinput = "";
                    placeText(iinput, 390, 600, 30);
                }
            }
        }
    }
    //add 10 points for score
    public static void addPoint()
    {
        scoreP += 10;
    }

    //remove heart whenever called
    public static void removeHeart()
    {
        //if the player got hit and loss a life, then remove the third heart
        if(plane.removeMe == 2)
        {
            removeH3 = true;
        }
        //if the player got hit and loss a life again, then remove the second heart
        if(plane.removeMe == 1)
        {
            removeH2 = true;
        }
        //if the player got hit and loss one more life, then remove the first heart, 
        //which is the last heart, therefore the game also ends
        if(plane.removeMe == 0)
        {
            removeH1 = true;
            gameOver = true;
        }

    }

    public void placeText(String text, int x, int y, int size)
    {
        if (getObjectsAt(x,y,Text.class).isEmpty()) 
        {
            this.addObject(new Text(text, size), x,y);
        }
        else 
        {
            getObjectsAt(x,y,Text.class).get(0).setImage(new GreenfootImage(text, size, Color.WHITE, TRANSPARENT));
        }
    }
    
    private void spawnEne()
    {
        if(rand == 1)
        {
            eX = Greenfoot.getRandomNumber(460)+20;//choose a x location for the enemy randomly 
            enemy = new Enemy();
            ene.add(enemy);
            addObject(enemy, eX, 0);
        }
    }
    
}
