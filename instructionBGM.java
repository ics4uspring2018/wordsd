import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * @author (WordsD) 
 * 
 * Description:
 * the background music for the instruction page (play only for the instruction world)
 */
public class instructionBGM extends Actor
{
    //soundtrack
    private static GreenfootSound m = new GreenfootSound("InstructionBGM.mp3");
    /**
     * Act - do whatever the BGM wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        // if the music is not playing the play it 
        if(!m.isPlaying())
        {
            m.play();
            m.setVolume(80);
        }
    }  
    
    public void stopS()
    {
        //stop the music
        m.stop();
    }
}
